<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clear Cache</title>
<link rel="shortcut icon" href="http://static.piriform.com/favicon.ico" />
</head>

<body>
<?php
function cleandir($dir) {

    if ($handle = opendir($dir)) {
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' && $file != '..' && is_file($dir.'/'.$file)) {
                if (unlink($dir.'/'.$file)) { }
                else { echo $dir . '/' . $file . ' (file) NOT deleted!<br />'; }
            }
            else if ($file != '.' && $file != '..' && is_dir($dir.'/'.$file)) {
                cleandir($dir.'/'.$file);
                if (rmdir($dir.'/'.$file)) { }
                else { echo $dir . '/' . $file . ' (directory) NOT deleted!<br />'; }
            }
        }
        closedir($handle);
    }

}


echo "****************** CLEARING CACHE ******************<br/>";

$dirs = array(
				"var/cache",
				//"var/session",
				"var/full_page_cache"
			);

foreach ($dirs as $dir) {
	if (file_exists($dir)) {
		echo "Clearing ".$dir."<br/>";
		cleandir($dir);
	}
}


?>

</body>
</html>
