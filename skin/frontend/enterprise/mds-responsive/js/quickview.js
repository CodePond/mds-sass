// JavaScript Document


(function($){
	$(document).ready(function(){
		$(".quick-view").on("click", function(){
			$(".quick-viewed").removeClass("quick-viewed");
			$(this).parent("li").addClass("quick-viewed");
		});

		$(document).delegate("#lightwindow_next", "click", function(){
			$(".quick-viewed").nextAll('li.simple-config:visible:first').find(".quick-view").trigger("click");
		});

		$(document).delegate("#lightwindow_previous", "click", function(){
			$(".quick-viewed").prevAll('li.simple-config:visible:first').find(".quick-view").trigger("click");
		});
		
	});
	})(jQuery);


	function quickviewproductid(id)
	{  loading();
	   	jQuery.ajax({
				type: "POST",
				cache: false,
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				url: id,
				success: function(json)
				{				
					jQuery("#lightwindow_contents").html(json.content);
					jQuery('#backgroundPopup').css('display', 'block');
	   				jQuery('#toPopup').css('display', 'block');
					jQuery("#toPopup").fadeIn(0500); // fadein popup div
	            			jQuery("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
	            			jQuery("#backgroundPopup").fadeIn(0001);

						jQuery('.product-shop').find('select').uniform();
					 if(jQuery(".quick-viewed").nextAll('li.simple-config:visible:first').length <= 0)
								{	
									jQuery("#lightwindow_next").hide();
								}
								else{
									jQuery("#lightwindow_next").show();
								}

								if(jQuery(".quick-viewed").prevAll('li.simple-config:visible:first').length <= 0)
								{	
									jQuery("#lightwindow_previous").hide();
								}else{
									jQuery("#lightwindow_previous").show();
								}



				}
			});
		}

	function loading() {
				jQuery("div#wait").show();
		    }


	jQuery(document).ready(function() {
		jQuery('#close').click(function (event) 
	    	{       jQuery("div#wait").hide();
			jQuery('#toPopup').css('display', 'none');
			jQuery("#toPopup").fadeOut("normal");
			jQuery("#backgroundPopup").fadeOut("normal");
		});
		jQuery("div#backgroundPopup").click(function() {
				jQuery("div#wait").hide();
			        jQuery('#toPopup').css('display', 'none');
				jQuery("#toPopup").fadeOut("normal");
				jQuery("#backgroundPopup").fadeOut("normal");
			    });
	});
		


(function($) {
	window.quickView = function(button, miniCartUrl){
		var productAddToCartForm = new VarienForm('product_addtocart_form_qv');
		if (productAddToCartForm.validator.validate()) {
			var form = jQuery(button).parents('#product_addtocart_form_qv');
			try {
				if (button && button != 'undefined') {
					button.disabled = true;
				}
				jQuery(button).next('#wait').show();
				jQuery.ajax({
					type: 'POST',
					url: form.attr('action'),
					data: form.serialize(),
					dataType: 'json',
					success: function(response) {
						if (response.success) {
							$("#messages_product_view").html(response.message);
							refreshMiniCart(miniCartUrl);
						} else {
							$("#messages_product_view").html(response.message);
						}
						jQuery(button).next('#wait').hide();
					},
					error: function() {
						$("#messages_product_view").html('We apologize but there was an error with your request.<br />Please try again later.');
						jQuery(button).next('#wait').hide();
					}
				});
			} catch (e) {
				throw e;
			}
	
			if (button && button != 'undefined') {
				button.disabled = false;
			}
	
			return false;
		}
	}
	
	/**
	 * Refreshes the minicart drop down
	 * @param miniCartUrl
	 */
	window.refreshMiniCart = function(miniCartUrl){
		jQuery.ajax({
			type: 'GET',
			url: miniCartUrl,
			dataType: 'html',
			success: function(response) {
				var $response = jQuery(response);
				var responseTitle = $response.find('#cartHeader');
				var responseContent = $response.find('#topCartContent').html();
				jQuery('#cartHeader').replaceWith(responseTitle);
				jQuery('#topCartContent').html(responseContent);
				
				// Below can be used to show minicart after item added
				window.setTimeout(delayResponse, 250);
				
				function delayResponse() {
					Enterprise.TopCart.showCart(5)
				}
				
			},
			error: function() {
				displayMessage('We apologize but there was an error with your request.<br />Please try again later.');
			}
		});
	}
})(jQuery);


