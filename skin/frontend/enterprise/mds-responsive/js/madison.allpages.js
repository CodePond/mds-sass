var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i) ? true : false;
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i) ? true : false;
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) ? true : false;
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
    }
};
    function viewport() {
                       var e = window, a = 'inner';
                       if (!('innerWidth' in window)) {
                           a = 'client';
                           e = document.documentElement || document.body;
                       }
                       return {width: e[ a + 'Width' ], height: e[ a + 'Height' ]};
                   }
                
    function checkMediaQuery(width){
		if ( navigator.appName != 'Microsoft Internet Explorer' ) {
			var mql = window.matchMedia("screen and (max-width: "+ width +"px)")
			return mql.matches;
		}else{
			return viewport().width <= width;
		}
    }
    
// JavaScript Document
(function($) {
    jQuery(document).ready(function() {
		
		 if (isMobile.any()) {
			 $(".quick-view").hide();
			 $('body').addClass('mobile');
		 }
		
		
	if(isMobile.Android()) {
		 $(document).delegate('.product-image-block', 'click', function() {
		  if($(this).hasClass('clicked')) {
		   return true; 
		  } else {
		   $('.product-image-block').removeClass('clicked');
		   $(this).addClass('clicked');
		   return false;
		  }
		 });
	}
window.onload = function(){
 corraLazy();
};

//Product list equal heights
equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = 

$(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height

(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = 

(currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load

(function() {
		equalheight('.product-view .box-related li.item');
		equalheight('.product-view .box-up-sell .products-grid li');
		equalheight('.catalog-product-compare-index .popup_catalog_product_compare');
});


$(window).resize(function(){

		equalheight('.product-view .box-related li.item');
		equalheight('.product-view .box-up-sell .products-grid li');
		equalheight('.catalog-product-compare-index .popup_catalog_product_compare'); 
});





// Uniform JS 
  
$("select, input:checkbox, input:radio").not('.no-uniform').uniform({selectAutoWidth: false});

$("select, input:checkbox, input:radio").not('.no-uniform').change( function(){
   $.uniform.update();
     });
  
  $( window ).resize(function() {
     $.uniform.update();
     corraLazy();
  });

        //Newsletter

        jQuery('form#newsletter-validate-detail input#newsletter').css('color', '#d8d8d8')
        jQuery('form#newsletter-validate-detail input#newsletter').blur(function() {
            if (jQuery(this).val() == "ENTER YOUR EMAIL ADDRESS") {
                jQuery(this).css('color', '#d8d8d8')
            } else {
                jQuery(this).css('color', '#636363')
            }
        })
        jQuery('form#newsletter-validate-detail input#newsletter').keydown(function() {
            jQuery(this).css('color', '#636363')

        })



        jQuery("ul#accordion li").toggle(function() {
            jQuery(this).addClass("active");
            jQuery(this).removeClass('plusimage')
        }, function() {
            jQuery(this).removeClass("active");
            jQuery(this).addClass('plusimage')
        });
        jQuery("ul#accordion li").click(function() {
            jQuery(this).children(".customerservice_content").slideToggle("slow");
        });

    });
})(jQuery);
Ajax.Responders.register({
    onComplete: function() {
       // jQuery.uniform.update();
	   jQuery("select, input:checkbox, input:radio").not('.no-uniform').uniform({selectAutoWidth: false});
    }
});