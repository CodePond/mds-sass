/*
 * Corra Lazy loading v1.0 - http://www.corra.com
 *
 * Copyright © 2013 CORRA
 * All rights reserved.
 * Author : Binu Chandran <binu@corra.com>
 * Created On : 21 Nov 2013
 */
var corraLazy = function(){
    
var $ = jQuery;

var    isScrolledIntoView = function (elem)
{
    
    $ = jQuery;
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop;

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

    var visibledLi = $(".item.show").length;
    var selector = ".item:visible";
        if(visibledLi > 0)
        {
            selector = ".item.show:visible:last ~ .item:visible";
        }
        
        $(selector).each(function(){
            if(isScrolledIntoView(this))
            {
                if($(this).find(".lazy").attr("src") == '' ){
                $(this).find(".lazy").hide();
                $(this).find(".lazy").attr("src", $(this).find(".lazy").attr("data-src"));
                $(this).find(".lazy").fadeIn(1000);
                
                }
            }
        });
};

jQuery(window).scroll(function() {
      corraLazy();
});