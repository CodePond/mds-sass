

(function($) {

    $.fn.tabs = function() {

        $('.tabs  .tab-content').css('display','none');
        $('.tabs .tab-list + .tab-content').attr('status','active').css('display','block');
        $('.tabs .tab-list .tab').not(':first-child').attr('status','inactive');
        $('.tabs .tab-list .tab:first-child').attr('status','active');
       
        
     
        $('.tab').click(function(event) {
            event.preventDefault();

           // var target_id = $(this).attr('href');
            $('.tab-content').not(this).css('display', 'none').attr('status','inactive');
            $($(this).attr('href')).css('display', 'block').attr('status','active');
           
           
            $(this).parent().children().not(this).attr('status','inactive');
            $(this).attr('status','active');
        });

    };

}(jQuery));