/*var browser;
jQuery.uaMatch = function (ua) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
        /(webkit)[ \/]([\w.]+)/.exec(ua) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
        /(msie) ([\w.]+)/.exec(ua) || ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) || [];

    return {
        browser: match[1] || "",
        version: match[2] || "0"
    };
};
// Don't clobber any existing jQuery.browser in case it's different
if (!jQuery.browser) {
    matched = jQuery.uaMatch(navigator.userAgent);
    browser = {};

    if (matched.browser) {
        browser[matched.browser] = true;
        browser.version = matched.version;
    }

    // Chrome is Webkit, but Webkit is also Safari.
    if (browser.chrome) {
        browser.webkit = true;
    } else if (browser.webkit) {
        browser.safari = true;
    }

    jQuery.browser = browser;
}*/

(function($) { //create closure so we can safely use $ as alias for jQuery
    $(document).ready(function() {
       /* if ((browser.msie && browser.version > 8) || !browser.msie) {
            $('.cms-mobile-nav').append($('.col-left.sidebar .cms-menu').clone());
        }*/

            // Iterate over each select element
            $('.cms-mobile-nav .cms-menu .parent > ol').each(function() {

                // Cache the number of options
                var $this = $(this),
                    numberOfOptions = $(this).children('li').length;

                // Hides the select element
                $this.addClass('s-hidden');

                // Wrap the select element in a div
                $this.wrap('<div class="cms-select"></div>');

                // Insert a styled div to sit over the top of the hidden select element
                $this.after('<div class="styledSelect"></div>');

                // Cache the styled div
                var $styledSelect = $this.next('div.styledSelect');

                // Show the first select option in the styled div
                $styledSelect.text($('.cms-mobile-nav .cms-menu .parent  ol >  li > strong').text());

                // Insert an unordered list after the styled div and also cache the list
                var $list = $('<ul />', {
                    'class': 'options'
                }).insertAfter($styledSelect);

                // Insert a list item into the unordered list for each select option
                for (var i = 0; i < numberOfOptions; i++) {

                    if (($this.children('li').eq(i).has('a').length > 0)) {
                        $('<li />', {
                            html: $this.children('li').eq(i).html(),
                            rel: $this.children('li').eq(i).val()
                        }).appendTo($list).click(function(e) {

                            window.location.href = $(e.target).children('a').eq(0).attr('href');

                        });

                    }

                }

                // Cache the list items
                var $listItems = $list.children('li');


                // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
                $styledSelect.click(function(e) {
                    //e.stopPropagation();
                    if ($(this).hasClass('active')) {
                        $('.styledSelect + .options').css('display', 'none')
                        //alert($('.styledSelect + .options').length);
                        $(this).removeClass('active');

                    }
                    else {
                        $('.styledSelect + .options').css('display', 'block')
                        //alert($('.styledSelect + .options').length);
                        $(this).addClass('active');

                    }
                    $('div.styledSelect.active').each(function() {

                    });

//$(this).toggleClass('active').next('ul.options').toggle();        
                });

                // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
                // Updates the select element to have the value of the equivalent option
                $listItems.click(function(e) {
                    //e.stopPropagation();
                    $styledSelect.text($(this).text()).removeClass('active');
                    $this.val($(this).attr('rel'));
                    $list.hide();
                    /* alert($this.val()); Uncomment this for demonstration! */
                });

                // Hides the unordered list when clicking outside of it
                $(document).click(function() {
                    //$styledSelect.removeClass('active');
                    //$list.hide();
                    // alert($(".styledSelect.active").length);
                });

                $(document).click(function() {
                    // $styledSelect.removeClass('active');
                    //$list.hide();

                });

            });

// Iterate over each select element
            $('#quick_search_type_id').each(function() {

                // Cache the number of options
                var $this = $(this),
                    numberOfOptions = $(this).children('option').length;

                // Hides the select element
                $this.addClass('s-hidden');

                // Wrap the select element in a div
                $this.wrap('<div class="skinned-select-wrapper"></div>');

                // Insert a styled div to sit over the top of the hidden select element
                $this.after('<div class="skinned-select"></div>');

                // Cache the styled div
                var $styledSelect = $this.next('div.skinned-select');

                // Show the first select option in the styled div
                $styledSelect.text($this.children('option').eq(0).text());

                // Insert an unordered list after the styled div and also cache the list
                var $list = $('<ul />', {
                    'class': 'options'
                }).insertAfter($styledSelect);

                // Insert a list item into the unordered list for each select option
                for (var i = 0; i < numberOfOptions; i++) {
                    $('<li />', {
                        text: $this.children('option').eq(i).text(),
                        rel: $this.children('option').eq(i).val()
                    }).appendTo($list);
                }

                // Cache the list items
                var $listItems = $list.children('li');

                // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
                $styledSelect.click(function(e) {

                    e.stopPropagation();
                    $('div.skinned-select.active').each(function() {
                        $(this).removeClass('active').next('ul.options').hide();
                    });
                    $(this).toggleClass('active').next('ul.options').toggle();
                });

                // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
                // Updates the select element to have the value of the equivalent option
                $listItems.click(function(e) {
                    e.stopPropagation();
                    $styledSelect.text($(this).text()).removeClass('active');
                    $this.val($(this).attr('rel'));
                    $list.hide();
                    //alert($(e.target).attr('rel'))
                    if($(e.target).attr('rel') === 'email'){
                        $('.form-row.email-row').css('display','inline-block');
                        $('.form-row.zipcode-row').css('display','none');
                    }
                    else if($(e.target).attr('rel') === 'zipcode'){
                        $('.form-row.email-row').css('display','none');
                        $('.form-row.zipcode-row').css('display','inline-block');
                    }
                    /* alert($this.val()); Uncomment this for demonstration! */
                });

                // Hides the unordered list when clicking outside of it
                $(document).click(function() {
                    $styledSelect.removeClass('active');
                    $list.hide();
                });

            });



            jQuery('.cms-input-text').on('click keyup', function(e) {
                $(e.target).prev('label').hide();
            });
            $('.cms-about .contact-us-form label').on('click', function(e) {
                $(e.target).hide();
                $(e.target).next('input').focus();
            });

            jQuery('.cms-input-text').on('blur', function(e) {
                if (e.target.value === "") {
                    $(e.target).prev('label').css('display', 'inline-block');
                }


            });

            $('.cms-input-text').prev('label').on('click',function(e){

                $(e.target).hide();
                $(e.target).next('input').focus();
            });

            $('.cms-input-text').each(function() {
                var myValue = $(this).val();
                if (myValue !== '') {
                    $(this).prev('label').hide();
                }
            });
        //}
    });
})(jQuery);