<?php

require_once 'Mage/Catalog/controllers/CategoryController.php';

class Corra_Json_CategoryController extends Mage_Catalog_CategoryController {

    public function productsAction() {
        if ($category = $this->_initCatagory()) {
        	$cache = Mage::app()->getCache();
            $val = $cache->load($category->getId());
            if ($val) {
                echo $val;
            } else {
                $layer = Mage::getSingleton('catalog/layer');
                $layer->getCurrentCategory()->setIsAnchor(1);
                $parentProducts = $layer->getProductCollection();
                $attributes = $layer->getFilterableAttributes(); 
				$mainproducts = array();
       
           $configProductAttr = array();
             
         
           foreach ($attributes as $attribute) 
		   { 
		    
			  if(in_array('configurable',explode(",",$attribute['apply_to'])))
			   {
			      $configProductAttr[]=$attribute['attribute_code'];
			   }
		   
		   }

                 foreach ($parentProducts as $parentProduct) {
                    $parentAttr = array();
                    $filterablevalues = array();
                    foreach ($configProductAttr as $key => $value) {
                      
                            $temp = array();
                            foreach (explode(",", $parentProduct->getData($value)) as $valueAtt) {
                                $temp[intval($valueAtt)] = 1;
                            }
                            $filterablevalues[$value] = $temp;
                            $parentAttr[] = $value;
                        
                    }
                    $filterablevaluesmain = array();
                    $pId = $parentProduct->getId();
                    if ($parentProduct->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE ) {


                        $products = $parentProduct->getTypeInstance(true)->getUsedProducts(null, $parentProduct);
                        foreach ($products as $product) {
                          
                          
                                foreach ($attributes as $attribute) {
                                    $attributeValue = $product->getData($attribute->getAttributeCode());
                                    if ($attributeValue /*&& !in_array($attribute->getAttributeCode(), $configProductAttr)*/) {
                                        $filterablevalues[$attribute->getAttributeCode()][round($attributeValue)] = 1;
                                    			}
                               				 }  
						
                            
                            $filterablevaluesmain[] = $filterablevalues;
                        } //simple product loop ends here
                    } //check for configurable products
                    elseif ($parentProduct->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE ) {

                       

                            foreach ($attributes as $attribute) {
                                $attributeValue = $parentProduct->getData($attribute->getAttributeCode());
                                if ($attributeValue) {
                                    $filterablevalues[$attribute->getAttributeCode()][round($attributeValue)] = 1;
                                }
                            }
                           $filterablevaluesmain[] = $filterablevalues;
                            
                       
                    }  //all other products if ends here

                    if (!empty($filterablevaluesmain)) {
                        $mainproducts[] = array("parentId" => $pId, "filterablevalues" => $filterablevaluesmain);
                    }
                }//product collection foreach ends


                $jsonvarriable = json_encode(array("products" => $mainproducts));
                echo $jsonvarriable; exit;
            }
        }
    }

}

?>