<?php
/**
 * Gallery  helper
 *
 * @category   Corra
 * @package    Corra_Gallery
 * @author     Ramesh
 */
 
class Corra_Gallery_Helper_Data extends Mage_Core_Helper_Abstract
{
  public function cmsfilter($data)
   {
      return Mage::helper('cms')->getBlockTemplateProcessor()->filter($data);
   }

}