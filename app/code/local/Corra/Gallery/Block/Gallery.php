<?php
/**
 * Gallery  block
 *
 * @category   Corra
 * @package    Corra_Gallery
 * @author     Ramesh
 */

class Corra_Gallery_Block_Gallery extends Mage_Core_Block_Template
{
    /**
     *Add block to cache
     *
     */

	protected function _construct()
	{
	$this->addData(array(
	'cache_lifetime' => 3600,
	'cache_tags'     => array(Corra_Gallery_Model_Mysql4_Gallery_Collection::CACHE_TAG),
	'cache_key'      => 'homepage_gallery',
	));
	}
	/**
     *Prepare Layout
     *
     */
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    /*
     *  Prepare gallery collection 
	 *  return collection
     */
 	
   public function gallerycollection()
    {     
        $collection = Mage::getModel('gallery/gallery')->getCollection();
	    $collection->addFieldToFilter('status',array(array('eq' => 1)));
        $collection->setOrder('sort_order', 'asc');
		return $collection;
		
  }
  /*
     *  check  module status (active/in active)
     */
  public function status()
   {
     return Mage::getStoreConfig('galleryopt/imgopt/active');
   }
  
}