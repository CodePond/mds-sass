<?php
/**
 * @category   Corratech
 * @package    Corratech_Gallery
 * @author     Ramesh
 */
 


class Corra_Gallery_Block_Adminhtml_Gallery_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
     $object = Mage::getModel('gallery/gallery')->load($this->getRequest()->getParam('id'));
     $object->getFilename();
	 if($object->getFilename())
	 {
	 $imafile=  '<div style="margin-top:10px"><h3>Image Preview</h3><br><img src="'.Mage::getBaseUrl('media').$object->getFilename().'" border="0" align="center" width="300"/></div>';
	 }
	 else
	 {
	 $imafile=  '<div style="margin-top:10px">No Preview Available</div>';

	 }
	 
      $configSettings = Mage::getSingleton('cms/wysiwyg_config')->getConfig(array( 'add_widgets' => true, 'add_variables' => true, 'add_images' => true,  'files_browser_window_url'=> $this->getBaseUrl().'admin/cms_wysiwyg_images/index/','directives_url'=>Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'))); 
      $form = new Varien_Data_Form();
      $this->setForm($form);	 
		  
      $fieldset = $form->addFieldset('gallery_form', array('legend'=>Mage::helper('gallery')->__('Item information')));     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('gallery')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));
      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('gallery')->__('File'),
          'required'  => false,
          'name'      => 'filename',
		  'after_element_html' =>$imafile ,
	  ));
	  
     $fieldset->addField('link', 'text', array(
          'label'     => Mage::helper('gallery')->__('URL link for image (Please add complete URL along with http://)'),
          'required'  => false,
          'name'      => 'link',
      ));
      $fieldset->addField('post_content', 'editor', array(
          'name'      => 'post_content',
          'label'     => Mage::helper('gallery')->__('Content'),
          'title'     => Mage::helper('gallery')->__('Content'),
          'style'     => 'width:700px; height:500px;',
		  'config'    => $configSettings,
          'wysiwyg'   => true,
          'required'  => false,
      ));
	  $fieldset->addField('position', 'select', array(
          'label'     => Mage::helper('gallery')->__('Content Rendering Position'),
          'name'      => 'position',
		  'values'    => array(
		      array(
                  'value'     => 'caption-default',
                  'label'     => Mage::helper('gallery')->__('Please select Content Rendering Position'),
              ),
              array(
                  'value'     => 'caption-top-left',
                  'label'     => Mage::helper('gallery')->__('Top - Left'),
              ),

              array(
                  'value'     => 'caption-top-right',
                  'label'     => Mage::helper('gallery')->__('Top - Right'),
              ),
              array(
                  'value'     => 'caption-bottom-left',
                  'label'     => Mage::helper('gallery')->__('Bottom - Left'),
              ),
			  array(
                  'value'     => 'caption-bottom-right',
                  'label'     => Mage::helper('gallery')->__('Bottom - Right'),
              ),
          ),
      ));
	  
	    $fieldset->addField('sort_order', 'text', array(
          'label'     => Mage::helper('gallery')->__('Position'),         
          'required'  => false,
          'name'      => 'sort_order',
      ));
	  
	   $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('gallery')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('gallery')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('gallery')->__('Disabled'),
              ),
          ),
      ));
	  
	  
	 
	 
      if ( Mage::getSingleton('adminhtml/session')->getGalleryData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getGalleryData());
          Mage::getSingleton('adminhtml/session')->setGalleryData(null);
      } elseif ( Mage::registry('gallery_data') ) {
          $form->setValues(Mage::registry('gallery_data')->getData());
      }
      return parent::_prepareForm();
  }
}