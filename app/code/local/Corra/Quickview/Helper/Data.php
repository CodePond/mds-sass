<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */
class Corra_Quickview_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected function _getUrl($path, $id = null, $type = null) 
	{
        $_secure 	= 	Mage::app()->getStore()->isCurrentlySecure();
        if (is_null($path)) 
		{
            return Mage::getUrl($path, array('_secure' => $_secure));
        }
        return Mage::getUrl($path, array('id' => $id, 'type' => $type, '_secure' => $_secure));
    }
	
	public function getQuickviewUrl($product)
	{
		return $this->_getUrl('catalog/quickview/view', $product->getId(), $product->getTypeId());
	}
	
	public function getQVUrl($product)
	{
		return $this->_getUrl('catalog/quickview/main', $product->getId(), $product->getTypeId());
	}

	public function getChildBlockNames( $blockNode ) 
	 {
        return array_unique( $this->_getChildBlockNames( $blockNode ) );
    }
	
	protected function _getChildBlockNames( $blockNode ) 
	{
        Varien_Profiler::start( 'quickview::helper::data::_getChildBlockNames' );
        if( $blockNode instanceof Mage_Core_Model_Layout_Element ) {
            $blockNames = array( (string)$blockNode['name'] );
            foreach( $blockNode->xpath( './block | ./reference' ) as $childBlockNode ) {
                $blockNames = array_merge( $blockNames,
                    $this->_getChildBlockNames( $childBlockNode ) );
            }
        } else {
            $blockNames = array();
        }
        Varien_Profiler::stop( 'quickview::helper::data::_getChildBlockNames' );
        return $blockNames;
    }
	
	 /**
     * @return Mage_Catalog_Model_Product or FALSE
     */
    public function getPreviousProduct()
    {
            $prodId 		= 	Mage::registry('product')->getId();
            $positions 		= 	Mage::getSingleton('core/session')->getQuickviewFilteredCategoryProductCollection();
            if (!$positions) 
			{
				$catArray 	= 	Mage::registry('current_category');
				if($catArray)
				{
					$positions 	= 	array_reverse(array_keys(Mage::registry('current_category')->getProductsPosition()));
				}
            }
            $cpk 			= 	@array_search($prodId, $positions);
            $slice 			= 	array_reverse(array_slice($positions, 0, $cpk));
            foreach ($slice as $productId) 
			{
				$product 	= 	Mage::getModel('catalog/product')->load($productId);
				if ($product && $product->getId() && $product->isVisibleInCatalog() && $product->isVisibleInSiteVisibility() && in_array($product->getTypeId(), array('simple', 'configurable'))) {
						return $product;
				}
            }
            return false;
    }
    /**
     * @return Mage_Catalog_Model_Product or FALSE
     */
    public function getNextProduct()
    {
            $prodId 		= 	Mage::registry('product')->getId();
            $positions 		= 	Mage::getSingleton('core/session')->getQuickviewFilteredCategoryProductCollection();
            if (!$positions) 
			{
				$catArray 	= 	Mage::registry('current_category');
				if($catArray)
				{
					$positions 	= 	array_reverse(array_keys(Mage::registry('current_category')->getProductsPosition()));
				}
            }
            $cpk 			= 	@array_search($prodId, $positions);
            $slice 			= 	array_slice($positions, $cpk + 1, count($positions));
            foreach ($slice as $productId) 
			{
				$product = Mage::getModel('catalog/product')->load($productId);
				if ($product && $product->getId() && $product->isVisibleInCatalog() && $product->isVisibleInSiteVisibility() && in_array($product->getTypeId(), array('simple', 'configurable'))) 
				{
					return $product;
				}
            }
            return false;
    }
	
	/**
     * @return string
     */
    public function getMiniCartUpdateUrl() 
	{
        return $this->_getUrl('catalog/quickview/minicartupdate/');
    }
	
	/**
     * @return string
     */
    public function getAjaxAddUrl() 
	{
        return $this->_getUrl('catalog/quickview/add/');
    }
}
?>
