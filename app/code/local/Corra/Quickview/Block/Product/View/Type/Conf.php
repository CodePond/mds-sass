<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */
class Corra_Quickview_Block_Product_View_Type_Conf extends Mage_Catalog_Block_Product_View_Type_Configurable
{
    protected $_productId 		= 	null;
    protected $_product 		= 	null;

    protected function _prepareLayout() 
	{
        return $this->getQuickProduct();
        //return parent::_prepareLayout();
    }

    public function getQuickProduct() 
	{
        if (!isset($this->_product)) 
		{
			if($this->getQuickProductId()!='')
			{
				$product 			= 	Mage::getModel('catalog/product')->load($this->getQuickProductId());
				$this->_product 	= 	$product;
				Mage::register('product', $product);
			} else {
				$this->_product 	= 	'';
			}
        }
        return $this->_product;
    }

    public function getQuickProductId() 
	{
        if (!isset($this->_productId)) 
		{
            $params 			= 	$this->getRequest()->getParams();
			if($params['id'])
            	$this->_productId 	= 	$params['id'];
			else
				$this->_productId 	= 	'';
        }
        return $this->_productId;
    }
	
    /**
     * Get JSON encripted configuration array which can be used for JS dynamic
     * price calculation depending on product options
     * <<<Stolen from Mage_Catalog_Block_Product_View>>>
     *
     * @return string
     */
    public function getJsonConfigPrice() {
        /* $config = array();
        if (!$this->hasOptions()) {
            return Mage::helper('core')->jsonEncode($config);
        }

        $_request = Mage::getSingleton('tax/calculation')->getRateRequest(false, false, false);
        $_request->setProductClassId($this->getProduct()->getTaxClassId());
        $defaultTax = Mage::getSingleton('tax/calculation')->getRate($_request);

        $_request = Mage::getSingleton('tax/calculation')->getRateRequest();
        $_request->setProductClassId($this->getProduct()->getTaxClassId());
        $currentTax = Mage::getSingleton('tax/calculation')->getRate($_request);

        $_regularPrice = $this->getProduct()->getPrice();
        $_finalPrice = $this->getProduct()->getFinalPrice();
        $_priceInclTax = Mage::helper('tax')->getPrice($this->getProduct(), $_finalPrice, true);
        $_priceExclTax = Mage::helper('tax')->getPrice($this->getProduct(), $_finalPrice);

        $config = array(
            'productId' => $this->getProduct()->getId(),
            'priceFormat' => Mage::app()->getLocale()->getJsPriceFormat(),
            'includeTax' => Mage::helper('tax')->priceIncludesTax() ? 'true' : 'false',
            'showIncludeTax' => Mage::helper('tax')->displayPriceIncludingTax(),
            'showBothPrices' => Mage::helper('tax')->displayBothPrices(),
            'productPrice' => Mage::helper('core')->currency($_finalPrice, false, false),
            'productOldPrice' => Mage::helper('core')->currency($_regularPrice, false, false),
            'skipCalculate' => ($_priceExclTax != $_priceInclTax ? 0 : 1),
            'defaultTax' => $defaultTax,
            'currentTax' => $currentTax,
            'idSuffix' => '_clone',
            'oldPlusDisposition' => 0,
            'plusDisposition' => 0,
            'oldMinusDisposition' => 0,
            'minusDisposition' => 0,
        );

        $responseObject = new Varien_Object();
        Mage::dispatchEvent('catalog_product_view_config', array('response_object' => $responseObject));
        if (is_array($responseObject->getAdditionalOptions())) {
            foreach ($responseObject->getAdditionalOptions() as $option => $value) {
                $config[$option] = $value;
            }
        }

        return Mage::helper('core')->jsonEncode($config); */

	$config = array();
    	if (!$this->hasOptions()) {
    		return Mage::helper('core')->jsonEncode($config);
    	}
    	
    	$_request = Mage::getSingleton('tax/calculation')->getRateRequest(false, false, false);
    	/* @var $product Mage_Catalog_Model_Product */
    	$product = $this->getProduct();
    	$_request->setProductClassId($product->getTaxClassId());
    	$defaultTax = Mage::getSingleton('tax/calculation')->getRate($_request);
    	
    	$_request = Mage::getSingleton('tax/calculation')->getRateRequest();
    	$_request->setProductClassId($product->getTaxClassId());
    	$currentTax = Mage::getSingleton('tax/calculation')->getRate($_request);
    	
    	$_regularPrice = $product->getPrice();
    	$_finalPrice = $product->getFinalPrice();
    	if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
    		$_priceInclTax = Mage::helper('tax')->getPrice($product, $_finalPrice, true,
    				null, null, null, null, null, false);
    		$_priceExclTax = Mage::helper('tax')->getPrice($product, $_finalPrice, false,
    				null, null, null, null, null, false);
    	} else {
    		$_priceInclTax = Mage::helper('tax')->getPrice($product, $_finalPrice, true);
    		$_priceExclTax = Mage::helper('tax')->getPrice($product, $_finalPrice);
    	}
    	$_tierPrices = array();
    	$_tierPricesInclTax = array();
    	foreach ($product->getTierPrice() as $tierPrice) {
    		$_tierPrices[] = Mage::helper('core')->currency($tierPrice['website_price'], false, false);
    		$_tierPricesInclTax[] = Mage::helper('core')->currency(
    				Mage::helper('tax')->getPrice($product, (int)$tierPrice['website_price'], true),
    				false, false);
    	}
    	$config = array(
    			'productId'           => $product->getId(),
    			'priceFormat'         => Mage::app()->getLocale()->getJsPriceFormat(),
    			'includeTax'          => Mage::helper('tax')->priceIncludesTax() ? 'true' : 'false',
    			'showIncludeTax'      => Mage::helper('tax')->displayPriceIncludingTax(),
    			'showBothPrices'      => Mage::helper('tax')->displayBothPrices(),
    			'productPrice'        => Mage::helper('core')->currency($_finalPrice, false, false),
    			'productOldPrice'     => Mage::helper('core')->currency($_regularPrice, false, false),
    			'priceInclTax'        => Mage::helper('core')->currency($_priceInclTax, false, false),
    			'priceExclTax'        => Mage::helper('core')->currency($_priceExclTax, false, false),
    			'skipCalculate'       => ($_priceExclTax != $_priceInclTax ? 0 : 1),
    			'defaultTax'          => $defaultTax,
    			'currentTax'          => $currentTax,
    			'idSuffix'            => '_clone',
    			'oldPlusDisposition'  => 0,
    			'plusDisposition'     => 0,
    			'plusDispositionTax'  => 0,
    			'oldMinusDisposition' => 0,
    			'minusDisposition'    => 0,
    			'tierPrices'          => $_tierPrices,
    			'tierPricesInclTax'   => $_tierPricesInclTax,
    	);
    	
    	$responseObject = new Varien_Object();
    	Mage::dispatchEvent('catalog_product_view_config', array('response_object' => $responseObject));
    	if (is_array($responseObject->getAdditionalOptions())) {
    		foreach ($responseObject->getAdditionalOptions() as $option => $value) {
    			$config[$option] = $value;
    		}
    	}
    	
    	return Mage::helper('core')->jsonEncode($config);	


    }
}

