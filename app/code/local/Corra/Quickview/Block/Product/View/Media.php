<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Simple product data view
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Corra_Quickview_Block_Product_View_Media extends Mage_Catalog_Block_Product_View_Media
{
	public function getCustomMediaGallary()
	{
		$_product 	= 	$this->getProduct();
 		$_helper 	= 	$this->helper('catalog/output');
		$mediaGallary=	'';
 		if (count($this->getGalleryImages()) > 0)
		{
			$i		=	0;
			foreach ($this->getGalleryImages() as $_image)
			{
				$_img 				= 	'<img id="image" src="'.$this->helper('catalog/image')->init($this->getProduct(), 'image', $_image->getFile())->resize(400, 300) .'" alt="'.$this->escapeHtml($this->getImageLabel()).'" title="'.$this->escapeHtml($this->getImageLabel()).'" />';
				$mainHtml 			= 	'<p class="product-image product-image-zoom  main-image">'.$_helper->productAttribute($_product, $_img, 'image').'</p>';
				$mediaGallary[$i]	=	$mainHtml;
				$i++;
			}
		}
		return $mediaGallary;
	}
}
