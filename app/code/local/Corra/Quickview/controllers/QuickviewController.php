<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

class Corra_Quickview_QuickviewController extends Mage_Core_Controller_Front_Action
{	
	public function getViewSimpleBlock()
    {
		$block = null;
        $layout = Mage::getSingleton( 'core/layout' );
        $layoutUpdate = $layout->getUpdate();
        $layoutUpdate->load( "catalog_quickview_simple" );
		$blockName = '';
        $layout->createBlock( 'Mage_Core_Block_Template', $blockName );
        $layout->generateXml();
        $blockNode = current( $layout->getNode()->xpath( sprintf( '//block[@name=\'%s\']', 'quickview' ) ) );
		
		if( $blockNode instanceof Varien_Simplexml_Element ) {
            $nodesToGenerate = Mage::helper( 'quickview/data' )
                ->getChildBlockNames( $blockNode );
            Mage::getModel( 'quickview/core_layout' )
                ->generateFullBlock( $blockNode );
            foreach( $nodesToGenerate as $nodeName ) {
                foreach( $layout->getNode()->xpath( sprintf(
                        '//reference[@name=\'%s\']', $nodeName ) ) as $node ) {
                    $layout->generateBlocks( $node );
                }
            }
            $block = $layout->getBlock( 'quickview' );
		}
		
        $layout->generateBlocks( $blockNode );
        $block = $layout->getBlock( 'quickview' );
	    //$block = $this->getLayout('quickview')->getOutput();
        return $block;
    }
	
	public function getViewConfBlock()
    {
		$block = null;
        $layout = Mage::getSingleton( 'core/layout' );
        $layoutUpdate = $layout->getUpdate();
        $layoutUpdate->load( "catalog_quickview_conf" );
		$blockName = '';
        $layout->createBlock( 'Mage_Core_Block_Template', $blockName );
        $layout->generateXml();
        $blockNode = current( $layout->getNode()->xpath( sprintf( '//block[@name=\'%s\']', 'quickview' ) ) );
		
		if( $blockNode instanceof Varien_Simplexml_Element ) {
            $nodesToGenerate = Mage::helper( 'quickview/data' )
                ->getChildBlockNames( $blockNode );
            Mage::getModel( 'quickview/core_layout' )
                ->generateFullBlock( $blockNode );
            foreach( $nodesToGenerate as $nodeName ) {
                foreach( $layout->getNode()->xpath( sprintf(
                        '//reference[@name=\'%s\']', $nodeName ) ) as $node ) {
                    $layout->generateBlocks( $node );
                }
            }
            $block = $layout->getBlock( 'quickview' );
		}
		
        $layout->generateBlocks( $blockNode );
        $block = $layout->getBlock( 'quickview' );
	    //$block = $this->getLayout('quickview')->getOutput();
        return $block;
    }
	
	public function mainAction()
    {
		$arrParams 					= 	$this->getRequest()->getParams();
		$frontOutput				=	array();
		if($arrParams['type'] == "simple")
		{
       		$block					=	$this->getViewSimpleBlock();
		} else {
       		$block					=	$this->getViewConfBlock();
		}
		//echo $block->toHtml();exit;
		//$newArray 		=  (array) $block;
		$frontOutput['content']		=	$block->toHtml();
		//$data						=	$block->getProduct()->getData();
		//echo $jsonData 		= 	json_encode($frontOutput, JSON_UNESCAPED_SLASHES);
  		$this->getResponse()->setHeader('Content-type', 'application/json');
		echo $jsonData   			=  json_encode($frontOutput);
        $this->getResponse()->setBody($jsonData);exit;
    }
	
    /**
     * Initialize product instance from request data
     *
     * @return Mage_Catalog_Model_Product || false
     */

	public function jsonviewAction()
    	{	$request = $this->getRequest();
		$data    = $request->getRawBody();
		$collection = json_decode($data);
		Mage::getSingleton('core/session')->setQuickviewFilteredCategoryProductCollection($collection);
		exit;	
		return;
	}

    protected function _initProduct()
    {
        $productId = (int) $this->getRequest()->getParam('product');
        if ($productId) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);
            if ($product->getId()) {
                return $product;
            }
        }
        return false;
    }
	
/**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }	
	
    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Add product to shopping cart action
     *
     * @return Mage_Core_Controller_Varien_Action
     * @throws Exception
     */
    public function addAction()
    {
        $cart   		= 	$this->_getCart();
        $params 		= 	$this->getRequest()->getParams();
        $json_error 	= 	false;
        $json_success 	= 	false;
        $json_message 	= 	null;
		if (isset($params['qty'])) 
		{
			$filter = new Zend_Filter_LocalizedToNormalized(array('locale' => Mage::app()->getLocale()->getLocaleCode()));
			$params['qty'] = $filter->filter($params['qty']);
		}

        $product 		= 	$this->_initProduct();
		/**
		 * Check product availability
		 */
		if (!$product) {
			$json_error = true;
			$json_message = "Invalid product.";
		}
		
		if (!$json_error) 
		{
            try 
			{
				$cart->addProduct($product, $params);
            	$cart->save();
            	$this->_getSession()->setCartWasUpdated(true);

				/**
				 * @todo remove wishlist observer processAddToCart
				 */
				Mage::dispatchEvent('checkout_cart_add_product_complete',
					array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
				);
			} catch (Mage_Core_Exception $e) {
                $json_error = true;
                $json_message = $e->getMessage();
            }

			if (!$cart->getQuote()->getHasError()) 
			{
				$json_success = true;
				$json_message = $this->__('<div class="success-msg">%s was added to your shopping cart.</div>', Mage::helper('core')->htmlEscape($product->getName()));
			}
		}
		
		$result = array(
            "success" => $json_success,
            "error" => $json_error,
            "message" => $json_message
        );

        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'application/json');
        $response->setBody(json_encode($result));
        $response->sendResponse();
        exit();
    }
	
    public function minicartupdateAction() 
	{
        die($this->getLayout()->createBlock('checkout/cart_sidebar')->setTemplate('checkout/cart/cartheader.phtml')->toHtml());
    }
}
