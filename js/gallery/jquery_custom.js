 
jQuery(document).ready(function(){ 

 
// HomePage Main full width Slider
jQuery("#fullslider").responsiveSlides({
      auto: true,
      nav: true,
      speed: 800,            
				  timeout: 4000,      // Integer: Time between slide transitions, in milliseconds
      namespace: "callbacks",
						pause: true,  
	     pager: true
								
	  });	
									

				 jQuery("#fullslider").swipe( {
        swipeLeft:function() {
              jQuery('.next').trigger('click');
        },
        swipeRight:function() {
              jQuery('.prev').trigger('click');
        },
        threshold:0
      });
	
  
 });				
 
 